import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /**
    * Main method.
    */
   public static void main(String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /**
    * Actual main method to run examples and everything.
    */
   public void run() {
      // Näidis:
      Graph sampleGraph = new Graph("H");
      Vertex v1 = sampleGraph.createVertex("v1");
      Vertex v2 = sampleGraph.createVertex("v2");
      Vertex v3 = sampleGraph.createVertex("v3");
      Vertex v4 = sampleGraph.createVertex("v4");
      Vertex v5 = sampleGraph.createVertex("v5");
      Vertex v6 = sampleGraph.createVertex("v6");
      Vertex v7 = sampleGraph.createVertex("v7");
      Vertex v8 = sampleGraph.createVertex("v8");
      sampleGraph.createArc("v1-v2", v1, v2);
      sampleGraph.createArc("v2-v3", v2, v3);
      sampleGraph.createArc("v3-v7", v3, v7);
      sampleGraph.createArc("v3-v4", v3, v4);
      sampleGraph.createArc("v4-v1", v4, v1);
      sampleGraph.createArc("v3-v5", v3, v5);
      sampleGraph.createArc("v5-v6", v5, v6);
      sampleGraph.createArc("v6-v1", v6, v1);
      sampleGraph.createArc("v1-v8", v1, v8);
      sampleGraph.createArc("v8-v6", v8, v6);
      // Jooksutame kõigi liikmete programme.
      sampleGraph.runAllPrograms(sampleGraph, v1, "v8", "v1");
   }

   /**
    * The task I chose was to determine whether or not the graph is weakly connected.
    * <p>
    * The algorithm that I came up with is the following:
    * 1. Convert directed graph to an undirected graph by exploring the graph using arcs (Arc class).
    * For example: Let's say we have a vertex A and a vertex B, both connected by an arc going from A to B.
    * We start from A and for every arc going out of it we check its target's arcs.
    * If one of the target's (in this case B) arcs point to our previous vertex (in this case A),
    * we do nothing. If it doesn't point back to it then we create an arc from B to A. We continue
    * doing so as long as we have vertexes left unvisited.
    * <p>
    * 2. Using adjacency matrix, determine recursively if all of the vertices are reachable from the first vertex.
    * Read the areVerticesConnected method javadoc for more information on this.
    * <p>
    * Output: Boolean whether the graph is weakly connected.
    */
   class Vertex {

      private final String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private int taskInfo = 0;
      // You can add more fields, if needed

      Vertex(String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public int getVInfo() {
         return info;
      }

      public void setVInfo(int i) {
         this.info = i;
      }

      public boolean hasNext() {
         return next != null;
      }

      public Vertex next() {
         return next;
      }
   }


   /**
    * Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;

      Arc(String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public boolean hasNext() {
         return next != null;
      }

      public Vertex getToVert() { // get target from arc (Vertex)
         return target;
      }

      public Arc next() { // get next arc
         return this.next;
      }
   }

   class Graph {

      private final String id;
      public Vertex first;
      private int info = 0;
      private int vNr;
      private ArrayList<ArrayList<Arc>> possibleRoutes = new ArrayList<>();
      public List<Arc> longest_path = Collections.synchronizedList(new LinkedList());

      Graph(String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph(String s) {
         this(s, null);
      }

      private void setGInfo(int i) {
         this.info = i;
      }

      public int getGInfo() {
         return this.info;
      }

      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuilder sb = new StringBuilder(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex(String vid) {
         Vertex res = new Vertex(vid);
         res.next = first;
         first = res;
         vNr++;
         return res;
      }

      public Arc createArc(String aid, Vertex from, Vertex to) {
         Arc res = new Arc(aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       *
       * @param n number of vertices added to this graph
       */
      public void createRandomTree(int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];
         for (int i = 0; i < n; i++) {
            varray[i] = createVertex("v" + (n - i));
            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               createArc("a" + varray[vnr].toString() + "_"
                       + varray[i].toString(), varray[vnr], varray[i]);
               createArc("a" + varray[i].toString() + "_"
                       + varray[vnr].toString(), varray[i], varray[vnr]);
            }
         }
      }

      /**
       * Same as above function, just adds an ability to choose ID label.
       */
      public void createRandomTree(int n, String label) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];
         for (int i = 0; i < n; i++) {
            varray[i] = createVertex(label + (n - i));
            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               createArc("a" + varray[vnr].toString() + "_"
                       + varray[i].toString(), varray[vnr], varray[i]);
               createArc("a" + varray[i].toString() + "_"
                       + varray[vnr].toString(), varray[i], varray[vnr]);
            }
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       *
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph(int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Same as above function, just adds an ability to choose ID label.
       */
      public void createRandomSimpleGraph(int n, int m, String label) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n, label);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Creates a small directional graph consisting of 2 vertices. The graph output is A<-B.
       * <p>
       * For testing purposes.
       */
      public void createDirectionalTwoVertexGraph() {
         vNr = 2;
         Vertex a = new Vertex("A");
         Vertex b = new Vertex("B");
         a.next = b;
         first = a;
         Arc arc = createArc(String.format("a%s_%s", b.toString(), a.toString()), b, a);
         b.first = arc;
      }

      /**
       * Creates a small disconnected graph consisting on 3 vertices and 1 arc.
       * <p>
       * A -> B    C
       * <p>
       * For testing purposes.
       */
      public void createSmallDisconnectedGraph() {
         vNr = 3;
         Vertex a = new Vertex("A");
         Vertex b = new Vertex("B");
         Vertex c = new Vertex("C");
         a.next = b;
         b.next = c;
         first = a;
         a.first = createArc(String.format("a%s_%s", a.toString(), b.toString()), a, b);
      }


      /**
       * Create a random big disconnected graph with a size on 4000 vertices and 5000 arcs.
       *
       * Uses the provided createRandomSimpleGraph() method.
       */
      public void createBigDisconnectedGraph() {
         // Doesn't matter if the initial graph is undirected or not since it will be converted into one anyway.
         createRandomSimpleGraph(2000, 2500);
         Graph g = new Graph("G1");
         g.createRandomSimpleGraph(2000, 2500, "c");
         Vertex v = first;
         while (v.next != null) {
            v = v.next;
         }
         v.next = g.first;
         vNr += g.vNr;
      }

      /**
       * Create a small directed graph with a size of 5 vertices and 5 arcs.
       */
      public void createSmallDirectedGraph() {
         vNr = 5;
         Vertex one = new Vertex("1");
         Vertex two = new Vertex("2");
         Vertex three = new Vertex("3");
         Vertex four = new Vertex("4");
         Vertex five = new Vertex("5");

         one.next = two;
         two.next = three;
         three.next = four;
         four.next = five;
         five.next = null;

         createArc("a1_2", one, two);
         createArc("a2_3", two, three);
         createArc("a3_2", three, two);
         createArc("a3_4", three, four);
         createArc("a5_4", five, four);

         first = one;
      }

      /**
       * If the graph from where this is called is directed, it returns the same graph converted to an undirected
       * graph. If the graph is already undirected it does nothing.
       *
       * @return Undirected graph
       */
      private Graph toUndirectedGraph() {
         Graph g = new Graph(this.id + "-ud", this.first);
         Vertex v = g.first;
         while (v != null) {
            Arc a = v.first;
            while (a != null) {
               Vertex t = a.target;
               List<String> tTargets = new ArrayList<>();
               Arc targetArc = t.first;
               while (targetArc != null) {
                  tTargets.add(targetArc.target.id);
                  targetArc = targetArc.next;
               }
               if (!tTargets.contains(v.id)) {
                  createArc(String.format("a%s_%s", t.toString(), v.toString()), t, v);
               }
               a = a.next;
            }
            v = v.next;
         }
         return g;
      }

      /**
       * Determine whether a graph is weakly connected or not by converting the graph to an undirected graph and then
       * checking whether the first vertex has a path to every other vertex in the graph.
       *
       * @return Boolean of whether the graph is weakly connected or not.
       */
      public boolean isWeaklyConnected() {
         Graph g = toUndirectedGraph();
         int[][] adj = g.createAdjMatrix();
         for (int to = 0; to < vNr; to++) {
            if (!areVerticesConnected(adj, 0, to, new boolean[vNr])) return false;
         }
         return true;
      }

      /**
       * Somewhat similar to DFS? https://en.wikipedia.org/wiki/Depth-first_search
       *
       * Check if the vertex can reach another vertex using the adjacency matrix method provided in the original
       * program. It does so recursively.
       *
       * @param adjMatrix Adjacency matrix of the graph.
       * @param from      The vertex where the path starts from.
       * @param to        The vertex you want to reach.
       * @param visited   Boolean array for checking what vertexes have been visited so far.
       * @return Boolean of whether the 'from' vertex can reach the 'to' vertex.
       */
      private boolean areVerticesConnected(int[][] adjMatrix, int from, int to, boolean[] visited) {
         if (adjMatrix[from][to] == 1) {
            return true;
         }
         visited[from] = true;
         for (int i = 0; i < vNr; i++) {
            if (adjMatrix[from][i] == 1 && !visited[i]) {
               visited[i] = true;
               if (areVerticesConnected(adjMatrix, i, to, visited)) {
                  return true;
               }
            }
         }
         return false;
      }

      public List<Vertex> findAllFourLongPaths(Vertex v){
         if (v == null){
            throw new RuntimeException("No such vertex, it is Null. Probably you are choosing Vertex from empty Graph");
         }
         List<Vertex> stations = new ArrayList<>();
         stations.add(v);
         int count = v.taskInfo;

         Arc a = v.first;
         while (a != null) {
            v = a.target;
            v.taskInfo = count + 1;
            if (v.taskInfo == 5){
               break;
            }
            if (!stations.contains(v)) {
               stations.add(v);
            }
            for (Vertex ve : findAllFourLongPaths(v)){
               if (!stations.contains(ve)){
                  stations.add(ve);
               }
            }
            a = a.next;
         }

         return stations;
      }
        /*Method for finding all simple paths between two vertices in an
       oriented graph using depth-first search.
      @param currentVertex is the vertex where the code starts searching from
      @param startVertex is the vertex that the cycle has to pass. It is always the same
      @param route is the array that holds the arcs

      I got information from the following sources
      * https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html (Retrieved 25.11.2020)
      * https://www.tutorialspoint.com/data_structures_algorithms/depth_first_traversal.htm
       (Retrieved 25.11.2020)
      */

      public void findAllCycles(Vertex currentVertex, Vertex startVertex, ArrayList<Arc> route) {
         Arc currentArc = currentVertex.first;
         /*Checking if the vertices have more arcs*/
         if (currentArc != null && !currentVertex.equals(startVertex) && currentArc.target.first != null
                 && !(route.contains(currentArc.target.first))) {
            route.add(currentArc);
            /*Recursively searches route to the start vertex*/
            findAllCycles(currentArc.target, startVertex, route);
            route.remove(route.size() - 1);
            /*Checks if the current vertex has any more arcs*/
            while (currentArc.next != null && currentArc.target.first != null
                    && !(route.contains(currentArc.target.first))) {
               currentArc = currentArc.next;
               route.add(currentArc);
               /*Recursively searches route to the start vertex*/
               findAllCycles(currentArc.target, startVertex, route);
               route.remove(route.size() - 1);
            }
         }
         if (currentVertex.equals(startVertex)) {
            ArrayList<Arc> routeClone = (ArrayList<Arc>) route.clone();
            possibleRoutes.add(routeClone); /*Adds the cloned route to the list of routes*/
         }
      }

      /*This method finds the shortest cycle in an oriented graph that passes through @param startVertex.
      It does so by finding the vertices that the startVertex has arcs to and then finds all the routes
       back to the startVertex. The method then finds the shortest from the routes.
       @param startVertex is the vertex that the cycle has to pass through
       */

      public ArrayList<Arc> findShortestCycle(Vertex startVertex) {
         Arc currentArc = startVertex.first;

         try {
            /*Finds route from the first arc*/
            findAllCycles(startVertex.first.target, startVertex, new ArrayList<Arc>());
         }
         catch (NullPointerException | StackOverflowError e){
            throw new RuntimeException("No cycles that contain vertex " + startVertex +
                    " were found.");
         }
         /*Checks if the start vertex has any more arcs*/
         while (currentArc.next != null) {
            /*Finds route from the other arcs*/
            findAllCycles(currentArc.next.target, startVertex, new ArrayList<Arc>());
            currentArc = currentArc.next;
         }
         /*Minimum length is assigned the maximum value an integer can have*/
         int minLength = Integer.MAX_VALUE;
         ArrayList<Arc> shortestRoute = new ArrayList<Arc>();
         /*Finds the shortest route from all routes*/
         for (ArrayList<Arc> possibleRoute : possibleRoutes) {
            if (possibleRoute.size() < minLength) {
               minLength = possibleRoute.size();
               shortestRoute = possibleRoute;
            }
         }
         if (shortestRoute.isEmpty()){
            throw new RuntimeException("No cycles that contain vertex " + startVertex +
                    " were found.");
         }
         String first = shortestRoute.get(0).id.split("-")[0];
         shortestRoute.add(0, new Arc(startVertex + "-" + first, startVertex, null));
         return shortestRoute;
      }

      public String routeToString(ArrayList<Arc> shortestRoute) {
         if (shortestRoute.isEmpty()){
            throw new RuntimeException("Path is empty");
         }
         String startVertex = shortestRoute.get(0).id.split("-")[0];
         return"The shortest cycle that contains vertex "+startVertex +" is "+shortestRoute
                 + "\nIt's length is " + shortestRoute.size() +"\nThe graph viewed is"+ this;
      }

      /**
       * Topological sort of vertices.
       * Source: https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       *
       * @param source first vertex
       * @param last   last vertex
       * @return longest path between two vertex
       */
      public String topolSort(String source, String last) {
         boolean cycleFound = false;
         List<Vertex> order = new ArrayList<>();

         setGInfo(1); // count number of vertices
         Vertex vit = this.first;
         while (vit.hasNext()) {
            vit.next().setVInfo(0);
            setGInfo(getGInfo() + 1);
            vit = vit.next();
         }
         // two if statement for wrong inputs
         if (Integer.parseInt(source.substring(1)) > getGInfo()) {
            throw new IllegalArgumentException("Wrong source vertex!");
         }
         if (Integer.parseInt(source.substring(1)) < Integer.parseInt(last.substring(1))) {
            throw new IllegalArgumentException("Source vertex should be bigger than last vertex!");
         }
         vit = this.first;

         while (true) {
            Arc ait = vit.first;
            while (true) {
               if (ait == null) {
                  vit = vit.next();
                  break;
               }
               if (ait.target != null) {
                  Vertex v = ait.getToVert();
                  // count number of incoming edges
                  v.setVInfo(v.getVInfo() + 1);
               }
               if (!ait.hasNext()) {
                  break;
               }
               ait = ait.next();
            }
            if (!vit.hasNext()) {
               break;
            }
            vit = vit.next();
         }
         List start = Collections.synchronizedList(new LinkedList());
         vit = this.first;
         while (true) {
            if (vit.getVInfo() == 0) {
               start.add(vit); // no incoming edges
            }
            if (!vit.hasNext()) {
               break;
            }
            vit = vit.next();
         }
         if (start.size() == 0) cycleFound = true;
         while ((!cycleFound) & (start.size() != 0)) {
            Vertex current = (Vertex) start.remove(0); // first vertex
            order.add(current);
            Arc ait = current.first;
            if (ait == null) {
               break;
            }
            while (ait.target != null) {
               Vertex v = ait.getToVert();
               v.setVInfo(v.getVInfo() - 1);
               if (v.getVInfo() == 0) {
                  start.add(v); // no incoming edges anymore
               }
               if (!ait.hasNext()) {
                  break;
               }
               ait = ait.next();
            }
         }
         if (getGInfo() != order.size()) cycleFound = true;
         if (cycleFound) {
            return "Cycle was found!"; // if cycle was found then we can not do topology sort
         }
         setGInfo(0);
         return findLongestPath(order, source, last);
      } // end of topolSort()

      /**
       * Search longest path in topologically sorted list.
       *
       * @param order  List of topologically sorted Vertex
       * @param source first vertex
       * @param last   last vertex
       * @return representation of path as a string.
       */
      private String findLongestPath(List<Vertex> order, String source, String last) {
         String result = "Longest directed path: ";
         Collections.reverse(order);
         List path = Collections.synchronizedList(new LinkedList());
         boolean is_start = false;
         for (Vertex v : order) {
            if (v.id.equals(last)) {
               is_start = true;
               path.add(v);
               continue;
            }
            if (is_start) {
               if (v.id.equals(source)) {
                  path.add(v);
                  Collections.reverse(path);
                  createArcPath(path); // create path of Arc for this Graph
                  result += showLongestPath(path);
                  return result;
               }
               Arc arc = v.first;
               Vertex target = (Vertex) path.get(path.size() - 1);
               while (true) {
                  if (arc.target == target) {
                     path.add(v);
                     break;
                  }
                  if (!arc.hasNext()) {
                     break;
                  }
                  arc = arc.next();
               }
            }
         }
         throw new IllegalArgumentException("Can not build longest path because last vertex is higher then first vertex!");
      }

      /**
       * Create longest path as List of Arc for Graph.
       */
      private void createArcPath(List<Vertex> path) {
         for (int i = 0; i < path.size() - 1; i++) {
            Vertex v = path.get(i);
            Arc arc = v.first;
            Vertex target = path.get(i + 1);
            while (true) {
               if (arc.target == target) {
                  longest_path.add(arc);
                  break;
               }
               if (!arc.hasNext()) {
                  break;
               }
               arc = arc.next();
            }
         }
      }

      /**
       * Represent longest path from List<Vertex>.
       */
      private String showLongestPath(List<Vertex> path) {
         String result = "";
         for (Vertex v : path) {
            if (path.get(path.size() - 1) == v) {
               result += v.id;
               return result;
            }
            result += v.id + " --> ";
         }
         throw new IllegalArgumentException("Can not build longest path because last vertex is higher then first vertex!");
      }

      private void runAllPrograms(Graph sampleGraph, Vertex startVertex, String topolSortSource, String topolSortLast) {
         try {
            System.out.println("The shortest cycle found in findShortestCycle containing vertex " +startVertex+ " is " + sampleGraph.findShortestCycle(startVertex));
         }
         catch (RuntimeException e) {
            System.out.println("No cycles that contain vertex " + startVertex +
                    " were found.");
         }
         System.out.println("All four long paths from vertex v1 are: " + sampleGraph.findAllFourLongPaths(sampleGraph.first.next));
         System.out.println("The longest path between " + topolSortSource +" and " + topolSortLast +": " + sampleGraph.topolSort(topolSortSource, topolSortLast));
         System.out.println("The given graph is weakly connected: " + sampleGraph.isWeaklyConnected());
         System.out.println("The graph is: " + sampleGraph);
      }
   }
}