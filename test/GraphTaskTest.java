import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (timeout=20000)
   public void kristjanBigDisconnectedGraph() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      g.createBigDisconnectedGraph();
      assertFalse("The graph should been disconnected.", g.isWeaklyConnected());
   }

   @Test (timeout=20000)
   public void kristjanSmallDisconnectedGraph() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      g.createSmallDisconnectedGraph();
      assertFalse("The graph should've been disconnected.", g.isWeaklyConnected());
   }

   @Test (timeout=20000)
   public void kristjanSmallConnectedGraph() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      g.createDirectionalTwoVertexGraph();
      assertTrue("The graph should've been weakly connected.", g.isWeaklyConnected());
   }

   @Test (timeout=20000)
   public void kristjanBigConnectedGraph() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      g.createRandomSimpleGraph(2000, 2500);
      assertTrue("The graph should've been weakly connected.", g.isWeaklyConnected());
   }

   @Test (timeout=20000)
   public void kristjanOneVertexGraph() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex v = task.new Vertex("A");
      g.first = v;
      assertTrue("The graph should've been weakly connected.", g.isWeaklyConnected());
   }

   @Test (timeout=20000)
   public void kristjanZeroVertexGraph() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      assertTrue("The graph should've been connected.", g.isWeaklyConnected());
   }
   @Test (timeout=20000)
   public void alekseiSingleVertexGraph(){
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      g.first = g.createVertex("A");
      assertArrayEquals(new GraphTask.Vertex[]{g.first}, g.findAllFourLongPaths(g.first).toArray());
   }
   @Test (timeout=20000)
   public void alekseiTwoVertexGraph(){
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex a = g.createVertex("A");
      GraphTask.Vertex b = g.createVertex("B");
      g.first = a;
      g.createArc("A-B", a, b);
      assertArrayEquals(new GraphTask.Vertex[]{a, b}, g.findAllFourLongPaths(g.first).toArray());
   }
   @Test (timeout=20000)
   public void alekseiOneWayLongRowOfVertexes(){
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex a = g.createVertex("A");
      GraphTask.Vertex b = g.createVertex("B");
      GraphTask.Vertex c = g.createVertex("C");
      GraphTask.Vertex d = g.createVertex("D");
      GraphTask.Vertex e = g.createVertex("E");
      GraphTask.Vertex f = g.createVertex("F");
      GraphTask.Vertex h = g.createVertex("H");
      GraphTask.Vertex i = g.createVertex("I");
      g.first = a;
      g.createArc("A-B", a, b);
      g.createArc("B-C", b, c);
      g.createArc("C-D", c, d);
      g.createArc("D-E", d, e);
      g.createArc("E-F", e, f);
      g.createArc("F-H", f, h);
      g.createArc("H-I", h, i);
      assertArrayEquals(new GraphTask.Vertex[]{a, b, c, d, e}, g.findAllFourLongPaths(g.first).toArray());
   }
   @Test (timeout=20000)
   public void alekseiBothWayLongRowOfVertexes(){
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex a = g.createVertex("A");
      GraphTask.Vertex b = g.createVertex("B");
      GraphTask.Vertex c = g.createVertex("C");
      GraphTask.Vertex d = g.createVertex("D");
      GraphTask.Vertex e = g.createVertex("E");
      GraphTask.Vertex f = g.createVertex("F");
      GraphTask.Vertex h = g.createVertex("H");
      GraphTask.Vertex i = g.createVertex("I");
      g.first = a;
      g.createArc("A-B", a, b);
      g.createArc("B-A", b, a);
      g.createArc("B-C", b, c);
      g.createArc("C-B", c, b);
      g.createArc("C-D", c, d);
      g.createArc("D-C", d, c);
      g.createArc("D-E", d, e);
      g.createArc("E-D", e, d);
      g.createArc("E-F", e, f);
      g.createArc("F-E", f, e);
      g.createArc("F-H", f, h);
      g.createArc("H-F", h, f);
      g.createArc("H-I", h, i);
      g.createArc("I-H", i, h);
      assertArrayEquals(new GraphTask.Vertex[]{a, b, c, d, e}, g.findAllFourLongPaths(g.first).toArray());
   }
   @Test (timeout=20000)
   public void alekseiBothWayLongRowOfVertexesStartingFromMiddle(){
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex a = g.createVertex("A");
      GraphTask.Vertex b = g.createVertex("B");
      GraphTask.Vertex c = g.createVertex("C");
      GraphTask.Vertex d = g.createVertex("D");
      GraphTask.Vertex e = g.createVertex("E");
      GraphTask.Vertex f = g.createVertex("F");
      GraphTask.Vertex h = g.createVertex("H");
      GraphTask.Vertex i = g.createVertex("I");
      g.first = a;
      g.createArc("A-B", a, b);
      g.createArc("B-A", b, a);
      g.createArc("B-C", b, c);
      g.createArc("C-B", c, b);
      g.createArc("C-D", c, d);
      g.createArc("D-C", d, c);
      g.createArc("D-E", d, e);
      g.createArc("E-D", e, d);
      g.createArc("E-F", e, f);
      g.createArc("F-E", f, e);
      g.createArc("F-H", f, h);
      g.createArc("H-F", h, f);
      g.createArc("H-I", h, i);
      g.createArc("I-H", i, h);
      assertArrayEquals(new GraphTask.Vertex[]{e,f,h,i,d,c,b,a}, g.findAllFourLongPaths(e).toArray());
   }

   @Test (expected = RuntimeException.class)
   public void mihkelSmallGraphNoCycle() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex A = g.createVertex("A");
      GraphTask.Vertex B = g.createVertex("B");
      GraphTask.Vertex C = g.createVertex("C");
      g.createArc("A-B", A, B);
      g.createArc("B-C", B, C);
      g.findShortestCycle(A);
   }

   @Test (timeout=20000)
   public void mihkelSmallGraphCycle() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex A = g.createVertex("A");
      GraphTask.Vertex B = g.createVertex("B");
      GraphTask.Vertex C = g.createVertex("C");
      GraphTask.Arc A1 = g.createArc("A-B", A, B);
      GraphTask.Arc A2 = g.createArc("B-C", B, C);
      GraphTask.Arc A3 = g.createArc("C-A", C, A);

      ArrayList<GraphTask.Arc> arcList = new ArrayList<GraphTask.Arc>();
      arcList.add(A1);
      arcList.add(A2);
      arcList.add(A3);
      String arcListString = arcList.toString();
      assertEquals("The arcs should have been -> " + arcListString, arcListString.compareTo(g.findShortestCycle(A).toString()), 0);
   }

   @Test (timeout=20000)
   public void mihkelBigGraphCycle() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex A = g.createVertex("A");
      GraphTask.Vertex B = g.createVertex("B");
      GraphTask.Vertex C = g.createVertex("C");
      GraphTask.Vertex D = g.createVertex("D");
      GraphTask.Vertex E = g.createVertex("E");
      GraphTask.Vertex F = g.createVertex("F");
      GraphTask.Vertex G = g.createVertex("G");
      GraphTask.Vertex H = g.createVertex("H");
      GraphTask.Vertex I = g.createVertex("I");
      GraphTask.Arc A1 = g.createArc("A-B", A, B);
      GraphTask.Arc A2 = g.createArc("B-C", B, C);
      GraphTask.Arc A3 = g.createArc("C-G", C, G);
      GraphTask.Arc A4 = g.createArc("C-D", C, D);
      GraphTask.Arc A5 = g.createArc("D-A", D, A);
      GraphTask.Arc A6 = g.createArc("C-E", C, E);
      GraphTask.Arc A7 = g.createArc("E-F", E, F);
      GraphTask.Arc A8 = g.createArc("F-A", F, A);
      GraphTask.Arc A9 =  g.createArc("A-H", A, H);
      GraphTask.Arc A10 = g.createArc("H-F", H, F);
      ArrayList<GraphTask.Arc> arcList = new ArrayList<GraphTask.Arc>();
      arcList.add(A9);
      arcList.add(A10);
      arcList.add(A8);
      String arcListString = arcList.toString();
      assertEquals("The arcs should have been -> " + arcListString, arcListString.compareTo(g.findShortestCycle(A).toString()), 0);
   }

   @Test (expected = RuntimeException.class)
   public void mihkelBigGraphNoCycle() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex A = g.createVertex("A");
      GraphTask.Vertex B = g.createVertex("B");
      GraphTask.Vertex C = g.createVertex("C");
      GraphTask.Vertex D = g.createVertex("D");
      GraphTask.Vertex E = g.createVertex("E");
      GraphTask.Vertex F = g.createVertex("F");
      GraphTask.Vertex G = g.createVertex("G");
      GraphTask.Vertex H = g.createVertex("H");
      GraphTask.Vertex I = g.createVertex("I");
      GraphTask.Arc A1 = g.createArc("A-B", A, B);
      GraphTask.Arc A2 = g.createArc("B-C", B, C);
      GraphTask.Arc A3 = g.createArc("C-G", C, G);
      GraphTask.Arc A4 = g.createArc("C-D", C, D);
      GraphTask.Arc A5 = g.createArc("D-A", D, A);
      GraphTask.Arc A6 = g.createArc("C-E", C, E);
      GraphTask.Arc A7 = g.createArc("E-F", E, F);
      GraphTask.Arc A8 = g.createArc("F-A", F, A);
      GraphTask.Arc A9 =  g.createArc("A-H", A, H);
      GraphTask.Arc A10 = g.createArc("H-F", H, F);
      g.findShortestCycle(G);
   }

   @Test (expected = RuntimeException.class)
   public void mihkelOneVertex() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex A = g.createVertex("A");
      g.findShortestCycle(A);
   }

   @Test (timeout=20000)
   public void nikitaSmallGraphTest() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex Ag = g.createVertex("v3");
      GraphTask.Vertex Bg = g.createVertex("v2");
      GraphTask.Vertex Cg = g.createVertex("v1");
      GraphTask.Arc v32 = g.createArc("v3-v2", Ag, Bg);
      GraphTask.Arc v21 = g.createArc("v2-v1", Bg, Cg);
      g.createArc("v3-v1", Ag, Cg);
      g.createArc("v1-v3", Cg, null); //empty arc for count
      g.topolSort("v3", "v1");
      List<GraphTask.Arc> longest_path = Collections.synchronizedList(new LinkedList());
      longest_path.add(v32);
      longest_path.add(v21);
      assertEquals(longest_path, g.longest_path);
   }

   @Test (timeout=20000)
   public void nikitaMediumGraphTest() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex Ag = g.createVertex("v5");
      GraphTask.Vertex Bg = g.createVertex("v4");
      GraphTask.Vertex Cg = g.createVertex("v3");
      GraphTask.Vertex Dg = g.createVertex("v2");
      GraphTask.Vertex Eg = g.createVertex("v1");
      GraphTask.Arc v54 = g.createArc("v5-v4", Ag, Bg);
      g.createArc("v5-v1", Ag, Eg);
      GraphTask.Arc v43 = g.createArc("v4-v3", Bg, Cg);
      GraphTask.Arc v32 = g.createArc("v3-v2", Cg, Dg);
      GraphTask.Arc v21 = g.createArc("v2-v1", Dg, Eg);
      g.createArc("v1-v2", Eg, null); //empty arc for count
      g.topolSort("v5", "v1");
      List<GraphTask.Arc> longest_path = Collections.synchronizedList(new LinkedList());
      longest_path.add(v54);
      longest_path.add(v43);
      longest_path.add(v32);
      longest_path.add(v21);
      assertEquals(longest_path, g.longest_path);
   }

   @Test (timeout=20000)
   public void nikitaBigGraphTest() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex Ag = g.createVertex("v10");
      GraphTask.Vertex Bg = g.createVertex("v9");
      GraphTask.Vertex Cg = g.createVertex("v8");
      GraphTask.Vertex Dg = g.createVertex("v7");
      GraphTask.Vertex Eg = g.createVertex("v6");
      GraphTask.Vertex Fg = g.createVertex("v5");
      GraphTask.Vertex Gg = g.createVertex("v4");
      GraphTask.Vertex Hg = g.createVertex("v3");
      GraphTask.Vertex Ig = g.createVertex("v2");
      GraphTask.Vertex Jg = g.createVertex("v1");
      GraphTask.Arc v109 = g.createArc("v10-v9", Ag, Bg);
      GraphTask.Arc v98 = g.createArc("v9-v8", Bg, Cg);
      GraphTask.Arc v87 = g.createArc("v8-v7", Cg, Dg);
      GraphTask.Arc v74 = g.createArc("v7-v4", Dg, Gg);
      GraphTask.Arc v64 = g.createArc("v6-v4", Eg, Gg);
      GraphTask.Arc v54 = g.createArc("v5-v4", Fg, Gg);
      GraphTask.Arc v43 = g.createArc("v4-v3", Gg, Hg);
      GraphTask.Arc v32 = g.createArc("v3-v2", Hg, Ig);
      GraphTask.Arc v21 = g.createArc("v2-v1", Ig, Jg);
      g.createArc("v1-v2", Jg, null); //empty arc for count
      g.topolSort("v10", "v1");
      List<GraphTask.Arc> longest_path = Collections.synchronizedList(new LinkedList());
      longest_path.add(v109);
      longest_path.add(v98);
      longest_path.add(v87);
      longest_path.add(v74);
      longest_path.add(v43);
      longest_path.add(v32);
      longest_path.add(v21);
      assertEquals(longest_path, g.longest_path);
   }

   @Test (expected = IllegalArgumentException.class)
   public void nikitaWrongInputTest() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex Ag = g.createVertex("v3");
      GraphTask.Vertex Bg = g.createVertex("v2");
      GraphTask.Vertex Cg = g.createVertex("v1");
      g.createArc("v3-v2", Ag, Bg);
      g.createArc("v2-v1", Bg, Cg);
      g.createArc("v3-v1", Ag, Cg);
      g.topolSort("v1", "v3"); // should be v3-v1
   }

   @Test (timeout=20000)
   public void nikitaCycleGraphTest() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex Ag = g.createVertex("v3");
      GraphTask.Vertex Bg = g.createVertex("v2");
      GraphTask.Vertex Cg = g.createVertex("v1");
      g.createArc("v3-v2", Ag, Bg);
      g.createArc("v2-v3", Bg, Ag); // cycle
      g.createArc("v2-v1", Bg, Cg);
      g.createArc("v3-v1", Ag, Cg);
      g.topolSort("v3", "v1");
      List<GraphTask.Arc> longest_path = Collections.synchronizedList(new LinkedList()); //empty list because cycle
      assertEquals(longest_path, g.longest_path);
   }
}

